function createNewUser(){
    let firstName = prompt('Enter your FirstName:');
    let lastName = prompt('Enter your LastName');
    let birthDay = prompt('Enter your day of birth in format: dd.mm.yyyy');
    let birthDayArray = birthDay.split('.');
    const newUser = {
        firstName,
        lastName,
        setFirstName(value){
            Object.defineProperty(newUser, 'firstName',{
                writable: true,});
            return this.firstName = value;
        },
        setLastName(value){
            Object.defineProperty(newUser, 'lastName',{
                writable: true,});
            return this.lastName = value;
        },
        getLogin() {
            let firstLetter = firstName.charAt(0).toLocaleLowerCase();
            lastName = lastName.toLocaleLowerCase();
            return firstLetter + lastName;
        },
        getAge(){
            return new Date().getFullYear() - this.birthDay.getFullYear();
        },
        getPassword(){
            return this.firstName.charAt(0).toLocaleUpperCase() + this.lastName.toLocaleLowerCase()+birthDayArray[2];
        }
    };
    Object.defineProperty(newUser,"lastName",{

        writable: false,
    });
    Object.defineProperty(newUser,"firstName",{

        writable: false,
    });
    return newUser;
}
const newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());


